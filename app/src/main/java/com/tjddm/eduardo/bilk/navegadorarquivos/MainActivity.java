package com.tjddm.eduardo.bilk.navegadorarquivos;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends Activity implements OnItemClickListener {
	private ListView lista;
	private Button botaoUp;
    private File raiz;
	private File diretorio;
	
	private List<Map<String, Object>> dadosLista = new ArrayList<Map<String, Object>>();

    @SuppressLint("ShowToast") @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lista = (ListView) findViewById(R.id.listView1);
        botaoUp = (Button) findViewById(R.id.button1);
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())){
        	raiz = Environment.getExternalStorageDirectory();
        	diretorio = raiz;
        }else{
        	Toast.makeText(this, "Não há armazemaneto disponível", Toast.LENGTH_SHORT).show();
        }
        populaLista();
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, items);
//        
//        lista.setAdapter(adapter);
        lista.setOnItemClickListener(this);
    }
    
    private void populaLista(){
        String[] items = diretorio.list();
    	dadosLista.clear();
        for (String item : items) {

            Map<String, Object> mapa = new HashMap<String, Object>();
            diretorio = new File(diretorio, item);

            if (diretorio.isDirectory()) {
                mapa.put("dir", "Diretório");
            } else {
                mapa.put("dir", "Arquivo");
            }
            mapa.put("item", item);
            dadosLista.add(mapa);
            diretorio = diretorio.getParentFile();
        }
    	String[] daOnde = {"dir", "item"};
    	int[] praOnde = {R.id.dir, R.id.text};
    	SimpleAdapter sAdapter = new SimpleAdapter(this,dadosLista,R.layout.list_item_custom_2_linhas,daOnde, praOnde);
        
        lista.setAdapter(sAdapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //id == R.id.action_settings ||
        return super.onOptionsItemSelected(item);
    }
    private void geraMsgBox(String arquivoSelecionado, Context contexto){
    	String mensagem;
    	File arquivo;
		arquivo = new File(diretorio.getAbsolutePath()+"/"+arquivoSelecionado);
		
		mensagem = 	"Diretório: "+ arquivo.getParent() +"\n"+
					"Nome do Arquivo: "+arquivo.getName()+"\n"+
					"Tamanho: "+arquivo.length()+" bytes"+"\n"+
					"Executar: "+arquivo.canExecute()+"\n"+
					"Ler: "+arquivo.canRead()+"\n"+
					"Escrever: "+arquivo.canWrite()+"\n";
		
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(contexto);		
		dialogBuilder.setMessage(mensagem)
					.setTitle("Infos do arquivo Selecionado")
					.setIcon(android.R.drawable.ic_dialog_info)
					.show();
    }
    private void checkBotaoUp(){
    	if (diretorio.equals(raiz)){
    		botaoUp.setVisibility(View.INVISIBLE);
    	}else{
    		botaoUp.setVisibility(View.VISIBLE);
    	}
    }
    public void doUp(View v){
    	diretorio = diretorio.getParentFile();
    	populaLista();
    	checkBotaoUp();
    	
    }


	@Override
	public void onItemClick(AdapterView<?> parent, View v, int pos, long id) {
		Map<String, Object> selecao = new HashMap<String, Object>();
		selecao = (Map<String, Object>) parent.getItemAtPosition(pos);
		String item = selecao.get("item").toString();
		diretorio = new File(diretorio, item);
		checkBotaoUp();
		if (diretorio.isDirectory()){
			populaLista();
		}else{
			geraMsgBox(item,this);
			diretorio = diretorio.getParentFile();
		}
		
		
			
	}
}
